<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

//    public $fillable = ['title', 'description'];
    protected $guarded = [];

    /**
     * @return string
     */
    public function profileImage(){
        if($this->image){
            $imagePath = $this->image;
        }
        else{
            $imagePath = 'profile/ICe5PWOTXXegBjnkaDiPKnwH4iQj3RtogtcaBGQR.png';
        }
        return '/storage/'.$imagePath;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class);
    }
}
