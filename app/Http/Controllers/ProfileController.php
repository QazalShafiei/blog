<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;


class ProfileController extends Controller
{

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(User $user)
    {
        if (auth()->user()){
            $follows = auth()->user()->following->contains($user->id);
        }
        else{
            $follows = false;
        }
        //Todo:add guest users roles
        $login = auth()->user()->profile;
        //for increase speed in  queries
        $postsCount = Cache::remember(
        'count.posts.' . $user->id,
        now()->addSeconds(30),
        function () use ($user){
            return $user->posts->count();
        });
        $followersCount = Cache::remember(
        'count.followers.' . $user->id,
        now()->addSeconds(30),
        function () use ($user){
            return $user->profile->followers->count();
        });
        $followingsCount = Cache::remember(
        'count.followings.' . $user->id,
        now()->addSeconds(30),
        function () use ($user){
            return $user->following->count();
        });

        return view('profiles.index', compact('user', 'follows', 'login', 'followersCount', 'followingsCount', 'postsCount'));
    }

    /**
     * @param Profile $user
     */
    public function edit(User $user)
    {
        return view('profiles.edit', compact('user'));
    }

    /**
     * @param Request $request
     * @param User $user
     */
    public function update(Request $request, User $user)
    {
        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'url' => 'url',
            'image' => ''
        ]);
        if (request('image')){
            $imagePath = request('image')->store('profile', 'public');
            $image = Image::make(public_path("/storage/{$imagePath}"))->fit(1000, 1000);
            $image->save();
        }
        else{
            $imagePath = $user->profile->image;
            $image = Image::make(public_path("/storage/{$imagePath}"))->fit(1000, 1000);
            $image->save();
        }
        if (auth()->user() != null) {
            auth()->user()->profile()->update(array_merge
            ($data,
            ['image' => $imagePath]
            ));
            return redirect('/profile/' . $user->id);
        }
        else {
            return redirect('/profile/' . $user->id);
        }
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getFollowerList( User $user)
    {
        $followersUsername = $user->profile->followers->pluck('username', 'id');
        return view('profiles.followerList', compact('followersUsername', 'user'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getFollowingsList(User $user)
    {
        $followingsUsername = $user->following->pluck('title', 'id');
        return view('profiles.followingList', compact('followingsUsername'));
    }
}
