@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img src="{{$user->profile->profileImage()}}" class="rounded-circle" style="width: 150px; height: 150px">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <div class="d-flex align-items-center pb-4">
                <div class="h4">{{$user->username}}</div>
{{--                this created by FollowButton.vue as a component    --}}
                    <follow-button user-id="{{ $user->id }}" follows="{{ $follows }}"></follow-button>
                </div>
                @if($login !== null)
                <a href="{{route('post.create')}}">Add New Post</a>
                @endif
            </div>
            @if($login !== null)
            <a href="{{route('profile.edit', $user->id)}}">Edit Profile</a>
            @endif
            <div class="d-flex">
                <div class="pr-4"><strong>{{ $postsCount }}</strong> posts</div>
                <a class="text-dark" href="{{route('profile.followers', $user->id)}}"> <div class="pr-4"><strong>{{ $followersCount }}</strong> followers</div></a>
                <a class="text-dark" href="{{route('profile.followings', $user->id)}}"> <div class="pr-4"><strong>{{ $followingsCount }}</strong> following</div></a>
            </div>
            <div class="pt-4 font-weight-bold">{{$user->profile->title}}</div>
            <div>{{$user->profile->description}}</div>
            <div><a href="google.com">{{$user->profile->url}}</a> </div>
        </div>
    </div>
    <div class="row pt-5">
        @foreach($user->posts as $post)
            <div class="col-4 pb-4">
                <a href="/post/{{$post->id}}">
                    <img src="/storage/{{$post->image}}" class="w-100" style="width: 150px; height: 150px">
                </a>
            </div>
        @endforeach

    </div>
</div>
@endsection
