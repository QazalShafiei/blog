<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<table class="table">
    <tbody>
    @foreach($followersUsername as $key => $followerUsername)
    <tr>
        <td><a href="{{route('profile.show', $key)}}" class="text-dark">{{$followerUsername}}</a></td>
    </tr>
    @endforeach
    </tbody>
</table>
