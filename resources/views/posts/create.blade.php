@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('post.store') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="row">
                        <h2>add new post</h2>
                    </div>
                    <div class="form-group row">
                        <label for="caption" class="col-md-4 col-form-label text-md-right">Post Caption</label>
                        <div class="col-md-6">
                            <input id="caption"
                                   type="text" class="form-control @error('caption') is-invalid @enderror"
                                   name="caption" value="{{ old('caption') }}" required autocomplete="caption"
                                   autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <label for="image" class="col-md-4 col-form-label text-md-right">Post image</label>
                        <div class="row">
                            <input type="file" class="form-control-file" id="image" name="image">
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="row pt-4">
                            <button class="btn btn-primary"> Add new Post</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>
@endsection
