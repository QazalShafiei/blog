<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/email', function (){
    return new \App\Mail\NewUserWelcomeMail();
});
//Axios routes
Route::post('follow/{user}', [\App\Http\Controllers\FollowsController::class, 'store']);
//Post routes
Route::get('/', [\App\Http\Controllers\PostController::class, 'index']);
Route::get('/post/create', [\App\Http\Controllers\PostController::class, 'create'])->name('post.create');
Route::post('/post/store', [\App\Http\Controllers\PostController::class, 'store'])->name('post.store');
Route::get('/post/{post}', [\App\Http\Controllers\PostController::class, 'show'])->name('post.show');

//Profile routes
Route::get('/profile/{user}', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile.show');
Route::get('/profile/edit/{user}', [\App\Http\Controllers\ProfileController::class, 'edit'])->name('profile.edit');
Route::patch('/profile/update/{user}', [\App\Http\Controllers\ProfileController::class, 'update'])->name('profile.update');

//followers/followings List Show
Route::get('/followers/{user}', [\App\Http\Controllers\ProfileController::class, 'getFollowerList'])->name('profile.followers');
Route::get('/followings/{user}', [\App\Http\Controllers\ProfileController::class, 'getFollowingsList'])->name('profile.followings');
